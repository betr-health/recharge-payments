<?php

namespace RechargePayments;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class RechargePaymentsClient {

    /**
     * @var ClientInterface
     */
    public $client;

    /**
     *  @var string
     */
    private $accessToken;

    /**
     * Constructor
     *
     * @param ClientInterface $client
     */
    public function __construct(string $accessToken) {
        $this->accessToken = $accessToken;

        $guzzle = new Client([
            'base_uri'  => 'https://api.rechargeapps.com/',
            'headers'   => [
                'Content-Type'              => 'application/json',
                'Accept'                    => 'application/json',
                'X-Recharge-Access-Token'   =>  $this->accessToken,
            ]
        ]);

        $this->client = $guzzle;
    }

    /**
     * Check the response status code.
     *
     * @param ResponseInterface $response
     * @param int $expectedStatusCode
     *
     * @throws \RuntimeException on unexpected status code
     */
    private function checkResponseStatusCode(ResponseInterface $response, $expectedStatusCode)
    {
        $statusCode = $response->getStatusCode();

        if ($statusCode !== $expectedStatusCode) {
            throw new \RuntimeException('API returned status code ' . $statusCode . ' expected ' . $expectedStatusCode);
        }
    }
}